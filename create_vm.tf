variable "vsphere_user" {
    type = "string"
    description = "vSphere user"
}
variable "vsphere_password" {
    type = "string"
    description = "Password for vSphere user"
}
variable "vsphere_server" {
    type = "string"
    description = "vSphere hostname"
}

provider "vsphere" {
    user            = "${var.vsphere_user}"
    password        = "${var.vsphere_password}"
    vsphere_server  = "${var.vsphere_server}"
}

data "vsphere_datacenter" "dc" {
    name            = "${var.vsphere_datacenter}"
}

data "vsphere_datastore" "datastore" {
    name            = "${var.vsphere_datastore}"
    datacenter_id   = "${var.vsphere_datacenter.dc.id}"
}
