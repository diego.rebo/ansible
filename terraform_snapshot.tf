terraform {
  required_providers {
    vsphere = {
      source = "hashicorp/vsphere"
      version = "1.12.0"
    }
  }
}

provider "vsphere" {
  user = ""
  password = ""
  vsphere_server = "vcenter.snoke.local"

  # If you have a self-signed cert
  allow_unverified_ssl = true
}

## Clone VM

resource "vsphere_virtual_machine_snapshot" "demo2" {
  virtual_machine_uuid             = "421a4a1a-e862-d837-8934-314c49765908"
  snapshot_name                    = "Terraform Test2"
  description                      = "Automatized snapshot test"
  memory                           = "true"
  quiesce                          = "true"
}
